#include "eigen-test-functions.h"

void eigen_vector_scale(Vector& u,const Vector& v,float s)
{
  u = s * v;
}

void eigen_vector_add(Vector& u,const Vector& v,const Vector& w)
{
  u = v + w;
}

void eigen_vector_dot(float& r,const Vector& v,const Vector& w)
{
  r = v.dot(w);
}

void eigen_matrix_scale(Matrix& u,const Matrix& v,float s)
{
  u = s * v;
}

void eigen_matrix_add(Matrix& u,const Matrix& v,const Matrix& w)
{
  u = v + w;
}

void eigen_matrix_mul(Matrix& u,const Matrix& v,const Matrix& w)
{
  u = v * w;
}

void eigen_matrix_vector(Vector& u,const Matrix& v,const Vector& w)
{
  u = v * w;
}
