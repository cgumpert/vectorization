#!/bin/bash

LOGDIR=${1%/}
SCALAR_SINGLE=$2
SCALAR_DOUBLE=$3
PACKED_128B_SINGLE=$4
PACKED_128B_DOUBLE=$5
PACKED_256B_SINGLE=$6
PACKED_256B_DOUBLE=$7

sep=$(awk 'BEGIN {printf "%\47.0f",1000}')
sep=`expr substr $sep 2 1`

printf "%-25s %20s %20s %10s %20s %20s %14s %14s\n" "test name" "cycles" "instructions" "inst/cycle" "packed" "scalar" "vectorization" "time"
for l in `ls -1 $LOGDIR/*.log`
do
    test_name=${l%%.log}
    test_name=${test_name##$LOGDIR/}
    cycles=`grep cycles $l | awk '{print $1}'`
    inst=`grep instructions $l | awk '{print $1}'`
    scalar_s=`grep $SCALAR_SINGLE $l | awk '{print $1}'`
    scalar_d=`grep $SCALAR_DOUBLE $l | awk '{print $1}'`
    p128s=`grep $PACKED_128B_SINGLE $l | awk '{print $1}'`
    p128d=`grep $PACKED_128B_DOUBLE $l | awk '{print $1}'`
    p256s=`grep $PACKED_256B_SINGLE $l | awk '{print $1}'`
    p256d=`grep $PACKED_256B_DOUBLE $l | awk '{print $1}'`
    t=`grep "time elapsed" $l | awk '{printf "%.4fs",$1}'`

    ipc=`echo $inst $cycles | sed -e "s/$sep//g" | awk '{printf "%.2f",$1/$2}'`
    scalar=`echo $scalar_s $scalar_d | sed -e "s/$sep//g" | awk '{printf "%\47d",$1 + $2}'`
    packed=`echo $p128s $p128d $p256s $p256d | sed -e "s/$sep//g" | awk '{printf "%\47d",$1 + $2 + $3 + $4}'`
    packed_fraction=`echo $p128s $p128d $p256s $p256d $scalar_s $scalar_d | sed -e "s/$sep//g" | awk '{printf "%.2f%%/%.2f%%",($1*4 + $3*8)*100/($1*4 + $3*8 + $5 + 1),($2*2 + $4*4)*100/($2*2 + $4*4 + $6 + 1)}'`
    printf "%-25s %20s %20s %10s %20s %20s %14s %14s\n" "$test_name" "$cycles" "$inst" "$ipc" "$packed" "$scalar" "$packed_fraction" "$t"
done

#-e r5302c7 -e r5301c7 -e r5308c7 -e r5304c7 -e r5320c7 -e r5310c7
