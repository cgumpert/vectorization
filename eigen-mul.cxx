#include <iostream>
#include <Eigen/Core>

int main()
{
  const unsigned int N = 8;
  const unsigned int M = 5;
  
  // VectorXf is a vector of floats, with dynamic size.
  Eigen::Matrix<float,Eigen::Dynamic,Eigen::Dynamic> u(N,N), v(N,M), w(M,N);
  //Eigen::Matrix<float,N,M> u, v, w;
  for(unsigned int i = 0; i < 1000000; ++i)
    u = v * w;

  std::cout << u << std::endl;
}
