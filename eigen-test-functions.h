#include <Eigen/Core>

#ifndef SIZE
#define SIZE 5
#endif

#ifdef DYNAMIC
typedef Eigen::Matrix<float,Eigen::Dynamic,1> Vector;
typedef Eigen::Matrix<float,Eigen::Dynamic,Eigen::Dynamic> Matrix;
#else
typedef Eigen::Matrix<float,SIZE,1> Vector;
typedef Eigen::Matrix<float,SIZE,SIZE> Matrix;
#endif

void eigen_vector_scale(Vector& u,const Vector& v,float s);
void eigen_vector_add(Vector& u,const Vector& v,const Vector& w);
void eigen_vector_dot(float& r,const Vector& u,const Vector& v);
void eigen_matrix_scale(Matrix& u,const Matrix& v,float s);
void eigen_matrix_add(Matrix& u,const Matrix& v,const Matrix& w);
void eigen_matrix_mul(Matrix& u,const Matrix& v,const Matrix& w);
void eigen_matrix_vector(Vector& u,const Matrix& v,const Vector& w);
