#include <stdlib.h>
#include <iostream>
#include <Eigen/Core>
#include "eigen-test-functions.h"

#ifndef SIZE
#define SIZE 5
#endif

#ifndef FUNCTION
static_assert(0,"function not defined");
#endif

int main(int argc,char* argv[])
{
  assert(argc >= 2);
  const unsigned long int toys = atoi(argv[1]);
  std::cout << toys << std::endl;
  float f = 3.5;
#ifdef DYNAMIC
  Eigen::Matrix<float,Eigen::Dynamic,1> a(SIZE), b(SIZE), c(SIZE);
  a = Eigen::Matrix<float,Eigen::Dynamic,1>::Ones(SIZE);
  b = Eigen::Matrix<float,Eigen::Dynamic,1>::Ones(SIZE);
  c = Eigen::Matrix<float,Eigen::Dynamic,1>::Ones(SIZE);
  Eigen::Matrix<float,Eigen::Dynamic,Eigen::Dynamic> u(SIZE,SIZE), v(SIZE,SIZE), w(SIZE,SIZE);
  u = Eigen::Matrix<float,Eigen::Dynamic,Eigen::Dynamic>::Ones(SIZE,SIZE);
  v = Eigen::Matrix<float,Eigen::Dynamic,Eigen::Dynamic>::Ones(SIZE,SIZE);
  w = Eigen::Matrix<float,Eigen::Dynamic,Eigen::Dynamic>::Ones(SIZE,SIZE);
#else
  Eigen::Matrix<float,SIZE,1> a, b, c;
  a = Eigen::Matrix<float,SIZE,1>::Ones();
  b = Eigen::Matrix<float,SIZE,1>::Ones();
  c = Eigen::Matrix<float,SIZE,1>::Ones();
  Eigen::Matrix<float,SIZE,SIZE> u, v, w;
  u = Eigen::Matrix<float,SIZE,SIZE>::Ones();
  v = Eigen::Matrix<float,SIZE,SIZE>::Ones();
  w = Eigen::Matrix<float,SIZE,SIZE>::Ones();
#endif
  
  for(unsigned int i = 0; i < toys; ++i)
  {
    FUNCTION;
  }
}
