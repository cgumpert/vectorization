BINDIR   = bin
OBJDIR   = obj
LOGDIR   = log
CXXFLAGS += -I/opt/eigen/3.3-beta1/ -std=c++14 -O2 -mavx -mfma -g
CXX      = $(shell which g++)

REPEAT  = 1
TOYS    = 1000
SIZES   = 5 10 15 50 64 100
TESTS   = vec-scale vec-add vec-dot matrix-scale matrix-add matrix-mul matrix-vec

RUN_TESTS   = $(addprefix run_,$(TESTS))
TMP         = $(foreach e,$(TESTS),$(addprefix $(e)-,fixed dyn))
EXECUTABLES = $(foreach e,$(TMP),$(addprefix $(e)-,$(SIZES)))

all: run_tests analyse

.PHONY: compile
compile: $(addprefix $(BINDIR)/,$(EXECUTABLES))

$(BINDIR)/vec-scale-fixed-%: OBJ=$(OBJDIR)/vec-scale-fixed-$*.o
$(BINDIR)/vec-scale-fixed-%: THIS_CXXFLAGS=$(CXXFLAGS) -DSIZE=$* -DFUNCTION="eigen_vector_scale(a,b,f)"
$(BINDIR)/vec-scale-fixed-%: eigen-test-functions.cxx main_template.cxx
	@mkdir -p $(BINDIR)
	@mkdir -p $(OBJDIR)
	$(CXX) $(THIS_CXXFLAGS) -c -o $(OBJ) eigen-test-functions.cxx
	$(CXX) $(THIS_CXXFLAGS) -o $@ $(OBJ) main_template.cxx

$(BINDIR)/vec-scale-dyn-%: OBJ=$(OBJDIR)/vec-scale-dyn-$*.o
$(BINDIR)/vec-scale-dyn-%: THIS_CXXFLAGS=$(CXXFLAGS) -DSIZE=$* -DDYNAMIC -DFUNCTION="eigen_vector_scale(a,b,f)"
$(BINDIR)/vec-scale-dyn-%: eigen-test-functions.cxx main_template.cxx
	@mkdir -p $(BINDIR)
	@mkdir -p $(OBJDIR)
	$(CXX) $(THIS_CXXFLAGS) -c -o $(OBJ) eigen-test-functions.cxx
	$(CXX) $(THIS_CXXFLAGS) -o $@ $(OBJ) main_template.cxx

$(BINDIR)/vec-add-fixed-%: OBJ=$(OBJDIR)/vec-add-fixed-$*.o
$(BINDIR)/vec-add-fixed-%: THIS_CXXFLAGS=$(CXXFLAGS) -DSIZE=$* -DFUNCTION="eigen_vector_add(a,b,c)"
$(BINDIR)/vec-add-fixed-%: eigen-test-functions.cxx main_template.cxx
	@mkdir -p $(BINDIR)
	@mkdir -p $(OBJDIR)
	$(CXX) $(THIS_CXXFLAGS) -c -o $(OBJ) eigen-test-functions.cxx
	$(CXX) $(THIS_CXXFLAGS) -o $@ $(OBJ) main_template.cxx

$(BINDIR)/vec-add-dyn-%: OBJ=$(OBJDIR)/vec-add-dyn-$*.o
$(BINDIR)/vec-add-dyn-%: THIS_CXXFLAGS=$(CXXFLAGS) -DSIZE=$* -DDYNAMIC -DFUNCTION="eigen_vector_add(a,b,c)"
$(BINDIR)/vec-add-dyn-%: eigen-test-functions.cxx main_template.cxx
	@mkdir -p $(BINDIR)
	@mkdir -p $(OBJDIR)
	$(CXX) $(THIS_CXXFLAGS) -c -o $(OBJ) eigen-test-functions.cxx
	$(CXX) $(THIS_CXXFLAGS) -o $@ $(OBJ) main_template.cxx

$(BINDIR)/vec-dot-fixed-%: OBJ=$(OBJDIR)/vec-dot-fixed-$*.o
$(BINDIR)/vec-dot-fixed-%: THIS_CXXFLAGS=$(CXXFLAGS) -DSIZE=$* -DFUNCTION="eigen_vector_dot(f,a,b)"
$(BINDIR)/vec-dot-fixed-%: eigen-test-functions.cxx main_template.cxx
	@mkdir -p $(BINDIR)
	@mkdir -p $(OBJDIR)
	$(CXX) $(THIS_CXXFLAGS) -c -o $(OBJ) eigen-test-functions.cxx
	$(CXX) $(THIS_CXXFLAGS) -o $@ $(OBJ) main_template.cxx

$(BINDIR)/vec-dot-dyn-%: OBJ=$(OBJDIR)/vec-dot-dyn-$*.o
$(BINDIR)/vec-dot-dyn-%: THIS_CXXFLAGS=$(CXXFLAGS) -DSIZE=$* -DDYNAMIC -DFUNCTION="eigen_vector_dot(f,a,b)"
$(BINDIR)/vec-dot-dyn-%: eigen-test-functions.cxx main_template.cxx
	@mkdir -p $(BINDIR)
	@mkdir -p $(OBJDIR)
	$(CXX) $(THIS_CXXFLAGS) -c -o $(OBJ) eigen-test-functions.cxx
	$(CXX) $(THIS_CXXFLAGS) -o $@ $(OBJ) main_template.cxx

$(BINDIR)/matrix-scale-fixed-%: OBJ=$(OBJDIR)/matrix-scale-fixed-$*.o
$(BINDIR)/matrix-scale-fixed-%: THIS_CXXFLAGS=$(CXXFLAGS) -DSIZE=$* -DFUNCTION="eigen_matrix_scale(u,v,f)"
$(BINDIR)/matrix-scale-fixed-%: eigen-test-functions.cxx main_template.cxx
	@mkdir -p $(BINDIR)
	@mkdir -p $(OBJDIR)
	$(CXX) $(THIS_CXXFLAGS) -c -o $(OBJ) eigen-test-functions.cxx
	$(CXX) $(THIS_CXXFLAGS) -o $@ $(OBJ) main_template.cxx

$(BINDIR)/matrix-scale-dyn-%: OBJ=$(OBJDIR)/matrix-scale-dyn-$*.o
$(BINDIR)/matrix-scale-dyn-%: THIS_CXXFLAGS=$(CXXFLAGS) -DSIZE=$* -DDYNAMIC -DFUNCTION="eigen_matrix_scale(u,v,f)"
$(BINDIR)/matrix-scale-dyn-%: eigen-test-functions.cxx main_template.cxx
	@mkdir -p $(BINDIR)
	@mkdir -p $(OBJDIR)
	$(CXX) $(THIS_CXXFLAGS) -c -o $(OBJ) eigen-test-functions.cxx
	$(CXX) $(THIS_CXXFLAGS) -o $@ $(OBJ) main_template.cxx

$(BINDIR)/matrix-add-fixed-%: OBJ=$(OBJDIR)/matrix-add-fixed-$*.o
$(BINDIR)/matrix-add-fixed-%: THIS_CXXFLAGS=$(CXXFLAGS) -DSIZE=$* -DFUNCTION="eigen_matrix_add(u,v,w)"
$(BINDIR)/matrix-add-fixed-%: eigen-test-functions.cxx main_template.cxx
	@mkdir -p $(BINDIR)
	@mkdir -p $(OBJDIR)
	$(CXX) $(THIS_CXXFLAGS) -c -o $(OBJ) eigen-test-functions.cxx
	$(CXX) $(THIS_CXXFLAGS) -o $@ $(OBJ) main_template.cxx

$(BINDIR)/matrix-add-dyn-%: OBJ=$(OBJDIR)/matrix-add-dyn-$*.o
$(BINDIR)/matrix-add-dyn-%: THIS_CXXFLAGS=$(CXXFLAGS) -DSIZE=$* -DDYNAMIC -DFUNCTION="eigen_matrix_add(u,v,w)"
$(BINDIR)/matrix-add-dyn-%: eigen-test-functions.cxx main_template.cxx
	@mkdir -p $(BINDIR)
	@mkdir -p $(OBJDIR)
	$(CXX) $(THIS_CXXFLAGS) -c -o $(OBJ) eigen-test-functions.cxx
	$(CXX) $(THIS_CXXFLAGS) -o $@ $(OBJ) main_template.cxx

$(BINDIR)/matrix-mul-fixed-%: OBJ=$(OBJDIR)/matrix-mul-fixed-$*.o
$(BINDIR)/matrix-mul-fixed-%: THIS_CXXFLAGS=$(CXXFLAGS) -DSIZE=$* -DFUNCTION="eigen_matrix_mul(u,v,w)"
$(BINDIR)/matrix-mul-fixed-%: eigen-test-functions.cxx main_template.cxx
	@mkdir -p $(BINDIR)
	@mkdir -p $(OBJDIR)
	$(CXX) $(THIS_CXXFLAGS) -c -o $(OBJ) eigen-test-functions.cxx
	$(CXX) $(THIS_CXXFLAGS) -o $@ $(OBJ) main_template.cxx

$(BINDIR)/matrix-mul-dyn-%: OBJ=$(OBJDIR)/matrix-mul-dyn-$*.o
$(BINDIR)/matrix-mul-dyn-%: THIS_CXXFLAGS=$(CXXFLAGS) -DSIZE=$* -DDYNAMIC -DFUNCTION="eigen_matrix_mul(u,v,w)"
$(BINDIR)/matrix-mul-dyn-%: eigen-test-functions.cxx main_template.cxx
	@mkdir -p $(BINDIR)
	@mkdir -p $(OBJDIR)
	$(CXX) $(THIS_CXXFLAGS) -c -o $(OBJ) eigen-test-functions.cxx
	$(CXX) $(THIS_CXXFLAGS) -o $@ $(OBJ) main_template.cxx

$(BINDIR)/matrix-vec-fixed-%: OBJ=$(OBJDIR)/matrix-vec-fixed-$*.o
$(BINDIR)/matrix-vec-fixed-%: THIS_CXXFLAGS=$(CXXFLAGS) -DSIZE=$* -DFUNCTION="eigen_matrix_vector(a,u,b)"
$(BINDIR)/matrix-vec-fixed-%: eigen-test-functions.cxx main_template.cxx
	@mkdir -p $(BINDIR)
	@mkdir -p $(OBJDIR)
	$(CXX) $(THIS_CXXFLAGS) -c -o $(OBJ) eigen-test-functions.cxx
	$(CXX) $(THIS_CXXFLAGS) -o $@ $(OBJ) main_template.cxx

$(BINDIR)/matrix-vec-dyn-%: OBJ=$(OBJDIR)/matrix-vec-dyn-$*.o
$(BINDIR)/matrix-vec-dyn-%: THIS_CXXFLAGS=$(CXXFLAGS) -DSIZE=$* -DDYNAMIC -DFUNCTION="eigen_matrix_vector(a,u,b)"
$(BINDIR)/matrix-vec-dyn-%: eigen-test-functions.cxx main_template.cxx
	@mkdir -p $(BINDIR)
	@mkdir -p $(OBJDIR)
	$(CXX) $(THIS_CXXFLAGS) -c -o $(OBJ) eigen-test-functions.cxx
	$(CXX) $(THIS_CXXFLAGS) -o $@ $(OBJ) main_template.cxx

libpfm4/perf_examples/evt2raw:
	cd libpfm4; make

.PHONY: perf_codes
perf_codes: libpfm4/perf_examples/evt2raw
	$(eval PACKED=$(shell ./libpfm4/perf_examples/evt2raw FP_ARITH:PACKED))
	$(eval PACKED_128B_SINGLE = $(shell ./libpfm4/perf_examples/evt2raw FP_ARITH:128B_PACKED_SINGLE))
	$(eval PACKED_128B_DOUBLE = $(shell ./libpfm4/perf_examples/evt2raw FP_ARITH:128B_PACKED_DOUBLE))
	$(eval PACKED_256B_SINGLE = $(shell ./libpfm4/perf_examples/evt2raw FP_ARITH:256B_PACKED_SINGLE))
	$(eval PACKED_256B_DOUBLE = $(shell ./libpfm4/perf_examples/evt2raw FP_ARITH:256B_PACKED_DOUBLE))
	$(eval SCALAR = $(shell ./libpfm4/perf_examples/evt2raw FP_ARITH:SCALAR))
	$(eval SCALAR_SINGLE = $(shell ./libpfm4/perf_examples/evt2raw FP_ARITH:SCALAR_SINGLE))
	$(eval SCALAR_DOUBLE = $(shell ./libpfm4/perf_examples/evt2raw FP_ARITH:SCALAR_DOUBLE))
	$(eval PERF_CMD=perf stat -r $(REPEAT) -e cycles -e instructions -e $(SCALAR_SINGLE) -e $(SCALAR_DOUBLE) -e $(PACKED_128B_SINGLE) \
			                -e $(PACKED_128B_DOUBLE) -e $(PACKED_256B_SINGLE) -e $(PACKED_256B_DOUBLE))
	@echo "pfm monitoring codes:"
	@printf "%-20s: %-8s\n" "SCALAR" "$(SCALAR)"
	@printf "%-20s: %-8s\n" "SCALAR_SINGLE" "$(SCALAR_SINGLE)"
	@printf "%-20s: %-8s\n" "SCALAR_DOUBLE" "$(SCALAR_DOUBLE)"
	@printf "%-20s: %-8s\n" "PACKED" "$(PACKED)"
	@printf "%-20s: %-8s\n" "PACKED_128B_SINGLE" "$(PACKED_128B_SINGLE)"
	@printf "%-20s: %-8s\n" "PACKED_128B_DOUBLE" "$(PACKED_128B_DOUBLE)"
	@printf "%-20s: %-8s\n" "PACKED_256B_SINGLE" "$(PACKED_256B_SINGLE)"
	@printf "%-20s: %-8s\n" "PACKED_256B_DOUBLE" "$(PACKED_256B_DOUBLE)"

.PHONY: $(RUN_TESTS)
$(RUN_TESTS): run_%: compile perf_codes
	@mkdir -p $(LOGDIR)
	@$(foreach i,$(filter $*%,$(EXECUTABLES)),echo "Running $(PERF_CMD) -o $(LOGDIR)/$(i).log $(BINDIR)/$(i) $(TOYS)";$(PERF_CMD) -o $(LOGDIR)/$(i).log $(BINDIR)/$(i) $(TOYS) > /dev/null;)

.PHONY: run_tests
run_tests: $(RUN_TESTS)

.PHONY: analyse
analyse: perf_codes
analyse:
	sh analyse.sh $(LOGDIR) $(SCALAR_SINGLE) $(SCALAR_DOUBLE) $(PACKED_128B_SINGLE) $(PACKED_128B_DOUBLE) $(PACKED_256B_SINGLE) $(PACKED_256B_DOUBLE)

.PHONY: clean
clean:
	rm -rf $(BINDIR)
	rm -rf $(OBJDIR)
	rm -rf $(LOGDIR)
